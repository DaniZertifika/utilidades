﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rambp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

            string ruta = @"\\192.168.1.197\c$\Temp\OCRoutPDF";
            int contador = 0;
            foreach (var file in Directory.GetFiles(ruta))
            {
                PdfReader pdf = new PdfReader(file);
                contador += pdf.NumberOfPages;
            }

            MessageBox.Show(contador.ToString() + " Páginas");



        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            byte[] imageArray = System.IO.File.ReadAllBytes(@"C:\Users\Daniel\Downloads\LOGO-ATM.jpg");
            string base64ImageRepresentation = Convert.ToBase64String(imageArray);
            MessageBox.Show(base64ImageRepresentation);
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            string resultado = "";
            foreach (var file in Directory.GetFiles(@"\\192.168.1.197\Temp\ANYWHERE2"))
            {
                resultado += file.ToString().Replace(@"\\192.168.1.197\Temp\ANYWHERE2\", "") + ", ";
            }
            foreach (var file in Directory.GetFiles(@"\\192.168.1.197\Temp\ANYWHERE"))
            {
                resultado += file.ToString().Replace(@"\\192.168.1.197\Temp\ANYWHERE\", "") + ", ";
            }

        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                string Destino = "lpueyo@zertifika.com";
                string Asunto = "Alta ";
                string Texto = CrearCuerpoCorreoAlta("Signes30",Destino);
                string Host = "mail.zertifika.com";
                string Origen = "info@zertifika.com";
                string Pass = "r3dM#z41#z41#z41";
                string User = "info@zertifika.com";
                MailMessage vCorreo = new MailMessage(Origen, Destino, Asunto, Texto);
                vCorreo.IsBodyHtml = true;

                //if (System.IO.File.Exists(Ruta))// && System.IO.File.Exists(Ruta2))
                //{
                //    Attachment vAdjunto = null;
                //    if (Ruta != null)
                //    {
                //        vAdjunto = new Attachment(Ruta);
                //        vCorreo.Attachments.Add(vAdjunto);
                //    }                

                SmtpClient vCliente = new SmtpClient(Host);
                vCliente.Credentials = new System.Net.NetworkCredential(User, Pass);

                vCliente.Send(vCorreo);

                MessageBox.Show("Mail enviado correctamente!");

            }
            catch (Exception)
            {
                MessageBox.Show("Error al enviar el mail.");
            }

        }

        public static string CrearCuerpoCorreoAlta(string Company, string Destino)
        {
            string _htmlheader = "<table width='98%' style='width: 98%;'>" +
                                       "<tr>" +
                                         "<td width='100%' style='width: 100%; padding: 30pt;'>" +
                                             "<table style='font-family: arial; font-size: 12px; width: 631px;'>" +
                                                 "<tr><td style='background: none repeat scroll 0% 0% #1f497d; padding-left:25px; height:32px; font-size:23px; font-family: Helvetica;'><p>" +
                                                    "<span style='color: white; letter-spacing: -0.35pt;'>" + Company + "<b><span style='color: white; font-size:9px;'></span></b>" +
                                                    // " <img src='"+ImagenEnBase64+"' height='70' width='140' style='margin-left:290px;margin-top:5px'></p>" +
                                                    //" <img src='https://www.signes30.com/ZDOCS/LOGO-ATM.jpg' height='70' width='140' style='margin-left:290px;margin-top:5px'></p>" +
                                                    "</tr>" +
                                                 "<tr>" +
                                                     "<td valign='top' style='border-width: medium 1pt 1pt; border-style: none solid solid; border-color: #385D8A; background: white; padding: 25px;' colspan='2'>";


            string _htmlSignature = "<div style='font-size:13px; padding: 10px 0 0 25px;'>";

            _htmlSignature += "<b><a style='color:#005f98; text-decoration: none;' href='https://www.signes30.com/ZDocspro/ResetPassword.aspx'>Enlace para establecer su Contraseña en Signes30</b></a>";

            _htmlSignature += "</div>";

            _htmlSignature += "<div style='border-width: medium medium 1pt; border-style: none none solid; border-color: rgb(204, 204, 204); padding: 0cm;'><p><span style='font-size: 9pt;'>&nbsp;</span></p></div><p><span style='font-size: 9pt;'></span></p></div>";

            string _htmlfooter = _mailFooter() +
                                          "</td>" +
                                        "</tr>" +
                                     "</table></td></tr></table>";


            return _htmlheader +"Ha sido dado de alta en "+ Company+", plataforma de gestión documental. </br></br> Su nombre de usuario es su email: "+Destino+" , para establecer su contraseña acceda al siguiente enlace" + _htmlSignature + _htmlfooter;
        }

        public static string _mailFooter()
        {
            string _footer = "<div style='font-size:10px; color: gray;'>";
            _footer += "<label><b>" + "Navegadores recomendados: Firefox, Chrome, Safari e Internet Explorer a partir de la versión 8." + "</b></label><br />";
            //_footer += "<label><b>" + _("Versión Beta Privada del servicio ZERTIFIKA.", c) + "</b></lable><br />";
            //_footer += "<label>" + _("Le informamos que temporalmente el ´servicio´ se proporciona en forma de versión beta privada ", c) + "</lable>";
            //_footer += "<label>" + _("y que se pone a disposición de los usuarios ´tal cual´ y en función de su disponibilidad, con la ", c) + "</lable>";
            //_footer += "<label>" + _("finalidad de proporcionar a ZERTIFIKA información acerca de la calidad y utilidad del mismo. El ", c) + "</lable>";
            //_footer += "<label>" + _("servicio puede contener errores o imprecisiones que pudieran provocar fallos, o incluso el borrado ", c) + "</lable>";
            //_footer += "<label>" + _("o pérdida de datos. Asimismo, el mantenimiento técnico y el soporte para el servicio, se realiza a ", c) + "</lable>";
            //_footer += "<label>" + _("través de medios online y en la página web.", c) + "</lable>";
            //_footer += "</div>";
            return _footer;
        }

    }
}
